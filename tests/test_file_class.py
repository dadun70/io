import os

os.path.realpath(os.path.dirname(__file__))


def test_init_function(test_file):
    assert test_file.name == "file_name", "Should get proper name"
    assert test_file.size == 1000, "Should get proper file size"
    assert test_file.dependencies == [], "Should get empty list of dependencies"
    assert test_file.functions == [], "Should get empty list of functions"


def test_add_dependency_function(test_file):
    test_file.add_dependency("file.py")
    assert len(test_file.dependencies) == 1, "Dependency list should get size 1 after adding 1 element"
    assert test_file.dependencies[0] == "file.py", "Should get new element on dependency list"


def test_add_functions_function(test_file):
    test_file.add_functions("function")
    assert len(test_file.functions) == 1, "Functions list should get size 1 after adding 1 element"
    assert test_file.functions[0] == "function", "Should get new element on functions list"
