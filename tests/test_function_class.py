import os

os.path.realpath(os.path.dirname(__file__))


def test_init_function(test_function):
    assert test_function.name == "function_name", "Should get proper name"
    assert test_function.functions == [], "Should get empty list of functions"


def test_add_functions__name_function(test_function):
    test_function.add_functions_names("function")
    assert len(test_function.functions) == 1, "Functions list should get size 1 after adding 1 element"
    assert test_function.functions[0] == "function", "Should get new element on functions list"
