import pytest
from File import File
from Function import Function
import os

file_path = os.path.dirname(os.path.abspath(__file__))


# pytest tests -v --cov-report term --cov-report html:htmlcov --cov-report xml --cov-fail-under=90 --cov=/home/lukasz/io/io

@pytest.fixture
def graph_structure_data():
    return [["file_name", 1000, "name2", 500, 1], ["name3", 400, "file_name", 1000, 1], ["name3", 400, "name4", 600, 1]]


@pytest.fixture
def compare_graph_structure_data():
    return [["file_name", 1000, "name2", 500, 1], ["name3", 400, "file_name", 1000, 1], ["name3", 400, "name4", 600, 1],
            ["name5", 330, "name3", 400, 1]]


@pytest.fixture
def positive_graph_structure_data():
    return [['file1', 20, 'file3', 25, 2], ['file1', 20, 'file2', 30, 2]]


@pytest.fixture
def positive_functions_graph_structure_data():
    return [['fun1', 1, 'fun2', 1, 1], ['fun1', 1, 'fun3', 2, 3], ['fun2', 1, 'fun3', 2, 1]]


@pytest.fixture
def test_file():
    return File("file_name", 1000)


@pytest.fixture
def test_function():
    return Function("function_name")


@pytest.fixture
def test_file_list():
    return [File("file1", 20, ["file3", "file2"], [Function("fun1", ["fun2", "fun3"])]),
            File("file2", 30, ["file1"], [Function("fun2", ["fun3"]), Function("fun3", ["fun1"])]),
            File("file3", 25, ["file1"], [Function("fun3", ["fun1"])])]


@pytest.fixture
def test_file_list_with_system_function():
    return [File("file1", 20, ["file3", "file2"], [Function("fun1", ["fun2", "fun3"]), Function("system_function")]),
            File("file2", 30, ["file1"], [Function("fun2", ["fun3"]), Function("fun3", ["fun1"])]),
            File("file3", 25, ["file1"], [Function("fun3", ["fun1"])])]


@pytest.fixture
def file_list_without_dependencies():
    return [File("file1", 20),
            File("file2", 30),
            File("file3", 25)]


@pytest.fixture
def test_function_list():
    return [Function("function1", ["function2", "function3"]), Function("function2", ["function4", "function3"]),
            Function("function3", ["function5"]), Function("function1", ["function2", "function3"])]


@pytest.fixture
def test_function_names_list():
    return ["function1", "function2", "function3"]


@pytest.fixture
def test_function_names_list_with_duplicate():
    return ["function1", "function2", "function3", "function1"]


@pytest.fixture
def test_file_names_list():
    return [file_path + '/test_directory/__init__.py', file_path + '/test_directory/test_file3.py',
            file_path + '/test_directory/test_file1.py', file_path + '/test_directory/test_file2.py',
            file_path + "/test_directory/test_nested_directory/__init__.py",
            file_path + "/test_directory/test_nested_directory/test_file4.py",
            file_path + "/test_directory/test_nested_directory/test_file5.py"]


@pytest.fixture
def test_reader_function_list():
    return [Function("test_function_1()", []), Function("test_function_2()", []), Function("test_function_3()", [])]


@pytest.fixture
def positive_function_file_data():
    return [['fun1', '', 'file1', '', ''],
            ['fun2', '', 'file2', '', ''],
            ['fun3', '', 'file2', '', ''],
            ['fun3', '', 'file3', '', '']]


@pytest.fixture
def positive_module_data():
    return [['file1', '', 'file2', '', 2],
            ['file2', '', 'file3', '', 1],
            ['file2', '', 'file1', '', 1],
            ['file3', '', 'file1', '', 1]]
