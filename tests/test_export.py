import pytest
import os
import Export

file_path = os.path.dirname(os.path.abspath(__file__))


@pytest.fixture
def export_xml(graph_structure_data):
    Export.exportXML("export", graph_structure_data)


def test_are_graph_files_created(export_xml):
    assert os.path.isfile("export.xml"), "Should create test_export_file.xml file"


def test_bad_data(test_file_list):
    with pytest.raises(TypeError):
        Export.exportXML("bad_data",
                         test_file_list), "After getting wrong list, should raise exception"


def teardown():
    if os.path.exists("export.xml"):
        os.remove("export.xml")
