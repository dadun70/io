# Tu wyszły testy bardziej integracyjne, ale nie wiem jak jednostkowe można zrobić dla takiej funkcji
import DrawGraph
import os.path
import pytest
import os

file_path = os.path.dirname(os.path.abspath(__file__))


def remove_compare_files_function():
    if os.path.exists(file_path[:-5] + "Comparison"):
        os.remove(file_path[:-5] + "Comparison")
    if os.path.exists(file_path[:-5] + "Comparison.svg"):
        os.remove(file_path[:-5] + "Comparison.svg")


@pytest.fixture
def draw_graph(graph_structure_data):
    DrawGraph.draw_graph(graph_structure_data, "test-version", "graph")


@pytest.fixture
def compare_and_draw_proper_file(compare_graph_structure_data):
    DrawGraph.compare_and_draw(compare_graph_structure_data, "new-version", "tests/test_graph_svg_data")


@pytest.fixture
def remove_compare_files():
    remove_compare_files_function()


def test_are_graph_files_created(draw_graph, test_file_list):
    assert os.path.isfile(file_path[:-5] + "graph"), "Should create graph.txt file"
    assert os.path.isfile(file_path[:-5] + "graph.svg"), "Should create graph.svg file"
    with pytest.raises(TypeError):
        DrawGraph.draw_graph(
            test_file_list, "test-version", "graph"), "After getting wrong list, should raise exception"


def test_graph_txt_file(draw_graph):
    with open(file_path[:-5] + "graph", "r") as test_file, open(file_path + "/test_graph_data.txt") as file:
        test_data = test_file.read()
        data = file.read()

        assert data == test_data, "Should create graph.txt file with proper content"


def test_compare_and_draw_function_with_proper_parameters(remove_compare_files, compare_and_draw_proper_file):
    # file_path[:-5] dlatego, ze pliki sa tworzone w folderze glownym
    assert os.path.isfile(
        file_path[:-5] + "Comparison"), "After getting proper parameters, should create Comparison.txt file"
    assert os.path.isfile(
        file_path[:-5] + "Comparison.svg"), "After getting proper parameters, should create Comparison.svg file"


def test_compare_and_draw_function_with_not_exist_file(remove_compare_files, compare_graph_structure_data):
    DrawGraph.compare_and_draw(compare_graph_structure_data, "new-version", "tests/not_exist_file")
    # file_path[:-5] dlatego, ze pliki sa tworzone w folderze glownym
    assert not os.path.isfile(
        file_path[
        :-5] + "Comparison"), "After getting not existing file as parameter, should not create Comparison.txt file"
    assert not os.path.isfile(
        file_path[
        :-5] + "Comparison.svg"), "After getting not existing file as parameter, should create Comparison.svg file"


def test_comparison_file_txt(remove_compare_files, compare_and_draw_proper_file):
    with open(file_path[:-5] + "Comparison", "r") as test_file, open(
            file_path + "/test_compare_data.txt", "r") as file:
        test_data = test_file.read()
        data = file.read()
        assert test_data == data, "Should create Comparison.txt file with proper content"


@pytest.mark.xfail(reason="gitlab CI environment")
def test_comparison_file_svg(remove_compare_files, compare_and_draw_proper_file):
    with open(file_path[:-5] + "Comparison.svg", "r") as test_file, open(
            file_path + "/test_compare_data_svg.svg", "r") as file:
        test_data = test_file.read()
        data = file.read()
        assert test_data == data, "Should create Comparison.svg file with proper content"


def test_draw_and_compare_with_bad_parameters(remove_compare_files, test_file_list):
    with pytest.raises(TypeError):
        DrawGraph.compare_and_draw(
            test_file_list, "test-version",
            "tests/test_graph_svg_data"), "After getting wrong list, should raise exception"


@pytest.mark.xfail(reason="gitlab CI environment")
def test_graph_svg_file(draw_graph):
    with open(file_path[:-5] + "graph.svg", "r") as test_file, open(
            file_path + "/test_graph_svg_data.svg", "r") as file:
        test_data = test_file.read()
        data = file.read()

        assert data == test_data, "Should create graph.svg file with proper content"


def teardown():
    if os.path.exists(file_path[:-5] + "graph"):
        os.remove(file_path[:-5] + "graph")
    if os.path.exists(file_path[:-5] + "graph.svg"):
        os.remove(file_path[:-5] + "graph.svg")
    remove_compare_files_function()
