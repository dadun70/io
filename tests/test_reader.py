import os
import Reader
import pytest

file_path = os.path.realpath(os.path.dirname(__file__))


def test_indent_pos_function():
    test_line = "line of testing text"
    test_line_with_indentation = "   line of testing text"
    assert Reader.indent_pos(test_line_with_indentation) == 3, "Should get size of indentation"
    assert Reader.indent_pos(test_line) == 0, "For line without indentation should get size 0"


def test_find_file_function(test_file_names_list):
    test_data = Reader.find_file(file_path + "/test_directory")
    assert len(test_data) == len(test_file_names_list), "Lists should get same size"
    for x in range(len(test_file_names_list)):
        if test_data[x] not in test_file_names_list:
            assert False
    else:
        assert True, "Should get proper files list"


def test_parse_file_function(test_reader_function_list):
    test_data = Reader.parse_file(file_path + "/test_file_to_parse.py")

    assert test_data.name == "test_file_to_parse.py", "Should get proper file name"
    assert test_data.size == os.path.getsize(file_path + "/test_file_to_parse.py"), "Should get proper file size"
    assert test_data.dependencies == ["os.py", "test_file1.py", "test_file2.py",
                                      "test_file3.py"], "Should get proper list of dependencies"
    for x in range(len(test_reader_function_list)):
        assert test_data.functions[x].name == test_reader_function_list[x].name
        assert test_data.functions[x].functions == test_reader_function_list[x].functions
