import os
import io
import Complexity
from contextlib import redirect_stdout

file_path = os.path.realpath(os.path.dirname(__file__))


def test_rank_files_function():
    assert Complexity.rank_fies("test_name",
                                5) == "test_name: 5              A (low risk - simple block)\n", "Should print proper statement for " \
                                                                                                 "number lower than, or equal 5"

    assert Complexity.rank_fies("test_name",
                                7) == "test_name: 7           B (low risk - well structured and stable block)\n", \
        "Should print proper statement for number higher than 5 and lower than 10"

    assert Complexity.rank_fies("test_name",
                                15) == "test_name: 15           C (moderate risk - slightly complex block)\n", \
        "Should print proper statement for number higher than 10 and lower than 20"

    assert Complexity.rank_fies("test_name",
                                25) == "test_name: 25           D (more than moderate risk - more complex block)\n", \
        "Should print proper statement for number higher than 20 and lower than 30"

    assert Complexity.rank_fies("test_name",
                                35) == "test_name: 35           E (high risk - complex block, alarming)\n", \
        "Should print proper statement for number higher than 30 and lower than 40"

    assert Complexity.rank_fies("test_name",
                                45) == "test_name: 45           F (very high risk - error-prone, unstable block)\n", \
        "Should print proper statement for number higher than 40"


def test_compute_cyclomatic_complexity_function():
    assert Complexity.compute_cyclomatic_complexity(
        file_path + "/test_file_to_compute_complexity.py") == 6, "For file with 6 decision block," \
                                                                 " should return their sum"
    assert Complexity.compute_cyclomatic_complexity(
        file_path + "/test_directory/test_file1.py") == 0, "Should return 0 for empty file"
