import os
import pytest
import Graph_service

file_path = os.path.realpath(os.path.dirname(__file__))


def test_find_duplicates_function(test_file, graph_structure_data):
    assert Graph_service.find_duplicates(graph_structure_data[0], test_file,
                                         "name2"), "Should return True after finding duplicate"
    assert not Graph_service.find_duplicates(graph_structure_data[0], test_file,
                                             "not_found_name"), "Should return False if duplicate hasn't been found"
    assert Graph_service.find_duplicates(graph_structure_data[1], test_file,
                                         "name3"), "Should work regardless of the order of the items"


def test_return_file_size_by_name_function(test_file_list):
    assert Graph_service.return_file_size_by_name(test_file_list, "file1") == 20, "Should get proper file size"
    assert Graph_service.return_file_size_by_name(test_file_list,
                                                  "not_found_name") is None, "Should get None if item isn't on the list"


def test_check_if_element_in_list_by_name_function(test_function_list):
    assert Graph_service.check_if_element_in_list_by_name(test_function_list,
                                                          "function1"), "Should get True if item" \
                                                                        "is on the list"
    assert not Graph_service.check_if_element_in_list_by_name(test_function_list,
                                                              "not_found_name"), "Should get False if item" \
                                                                                 "is not on the list"


def test_count_function_in_list_by_name_function(test_function_list):
    assert Graph_service.count_function_in_list_by_name(test_function_list,
                                                        "function1") == 2, "Should get proper number of occurrences"
    assert not Graph_service.count_function_in_list_by_name(test_function_list,
                                                            "not_found_name"), "Should get 0 if item" \
                                                                               "is not on the list"


def test_build_graph_structure_function(test_file_list, positive_graph_structure_data, file_list_without_dependencies,
                                        graph_structure_data):
    assert Graph_service.build_graph_structure(
        test_file_list) == positive_graph_structure_data, "Should get proper structure for classes dependency graph"
    assert Graph_service.build_graph_structure([]) == [], "After getting empty list, should return empty structure"
    assert Graph_service.build_graph_structure(
        file_list_without_dependencies) == [], "After getting files list without dependencies, " \
                                               "should return empty structure"
    with pytest.raises(AttributeError):
        Graph_service.build_graph_structure(graph_structure_data), "After getting wrong list, should raise exception"


def test_build_functions_graph_structure_function(test_file_list, positive_functions_graph_structure_data,
                                                  file_list_without_dependencies,
                                                  graph_structure_data, test_file_list_with_system_function):
    assert Graph_service.build_functions_graph_structure(
        test_file_list) == positive_functions_graph_structure_data, "Should get proper structure for functions " \
                                                                    "dependency graph"
    assert Graph_service.build_functions_graph_structure(
        []) == [], "After getting empty list, should return empty structure"
    assert Graph_service.build_functions_graph_structure(
        file_list_without_dependencies) == [], "After getting files list without dependencies, " \
                                               "should return empty structure"
    with pytest.raises(AttributeError):
        Graph_service.build_functions_graph_structure(
            graph_structure_data), "After getting wrong list, should raise exception"
    assert Graph_service.build_functions_graph_structure(
        test_file_list_with_system_function) == positive_functions_graph_structure_data, "Should ignore system functions"


def test_make_list_of_functions_function(test_function_names_list, test_function_names_list_with_duplicate):
    assert Graph_service.make_list_of_functions(
        test_function_names_list) == test_function_names_list, "If list doesn't include duplicates," \
                                                               " should return same data"
    assert Graph_service.make_list_of_functions(
        test_function_names_list_with_duplicate) == test_function_names_list, "If list include duplicates," \
                                                                              " should return data without duplicate"
    assert Graph_service.make_list_of_functions(
        []) == [], "After getting empty list, should return empty list"


def test_find_model_of_function(test_file_list):
    assert Graph_service.find_model_of_function(test_file_list, "fun1",
                                                "test") == "file1", "After getting proper data," \
                                                                    "should return proper file name"
    assert Graph_service.find_model_of_function(test_file_list, "not_found_fun",
                                                "file1") is None, "After getting function name which doesn't exist," \
                                                                  " should return None,"
    assert Graph_service.find_model_of_function(test_file_list, "fun2",
                                                "file1") == "file2", "Should skip item with name given " \
                                                                     "in third argument"
    assert Graph_service.find_model_of_function([], "fun2",
                                                "file1") is None, "After getting empty list, should return None,"


def test_build_functions_files_graph_structure_function(test_file_list, positive_function_file_data,
                                                        file_list_without_dependencies,
                                                        graph_structure_data):
    assert Graph_service.build_functions_files_graph_structure(
        test_file_list) == positive_function_file_data, "Should get proper structure for function class " \
                                                        "dependency graph"
    assert Graph_service.build_functions_files_graph_structure(
        []) == [], "After getting empty list, should return empty structure"
    assert Graph_service.build_functions_files_graph_structure(
        file_list_without_dependencies) == [], "After getting files list without dependencies, " \
                                               "should return empty structure"
    with pytest.raises(AttributeError):
        Graph_service.build_functions_files_graph_structure(
            graph_structure_data), "After getting wrong list, should raise exception"


def test_build_modules_graph_structure_function(test_file_list, positive_module_data,
                                                file_list_without_dependencies,
                                                graph_structure_data):
    assert Graph_service.build_modules_graph_structure(
        test_file_list) == positive_module_data, "Should get proper structure for module dependency graph"
    assert Graph_service.build_modules_graph_structure(
        []) == [], "After getting empty list, should return empty structure"
    assert Graph_service.build_modules_graph_structure(
        file_list_without_dependencies) == [], "After getting files list without dependencies, " \
                                               "should return empty structure"
    with pytest.raises(AttributeError):
        Graph_service.build_modules_graph_structure(
            graph_structure_data), "After getting wrong list, should raise exception"


def test_find_file_by_name_function(test_file_list):
    assert Graph_service.find_file_by_name(test_file_list, "file1").name == "file1", "After getting proper name should," \
                                                                                     " return right file"
    assert Graph_service.find_file_by_name(test_file_list,
                                           "not_found") is None, "After getting file name of not existing file," \
                                                                 " should return None"


def test_test_gather_three_versions_function_for_stories_1_3(remove_five_files, test_file_list, graph_structure_data,
                                                             positive_functions_graph_structure_data,
                                                             positive_module_data):
    # file_path[:-5] dlatego, ze pliki sa tworzone w folderze glownym
    Graph_service.gather_three_versions(test_file_list, graph_structure_data, positive_functions_graph_structure_data,
                                        positive_module_data, "13")

    assert os.path.isfile(
        file_path[:-5] + "five"), "After getting proper parameters, should create five.txt file"
    assert os.path.isfile(
        file_path[:-5] + "five.pdf"), "After getting proper parameters, should create Comparison.svg file"


def test_test_gather_three_versions_function_for_stories_2_3(remove_five_files, test_file_list, graph_structure_data,
                                                             positive_functions_graph_structure_data,
                                                             positive_module_data):
    # file_path[:-5] dlatego, ze pliki sa tworzone w folderze glownym
    Graph_service.gather_three_versions(test_file_list, graph_structure_data, positive_functions_graph_structure_data,
                                        positive_module_data, "23")

    assert os.path.isfile(
        file_path[:-5] + "five"), "After getting proper parameters, should create five.txt file"
    assert os.path.isfile(
        file_path[:-5] + "five.pdf"), "After getting proper parameters, should create Comparison.svg file"


@pytest.fixture
def remove_five_files():
    if os.path.exists(file_path[:-5] + "five"):
        os.remove(file_path[:-5] + "five")
    if os.path.exists(file_path[:-5] + "five.pdf"):
        os.remove(file_path[:-5] + "five.pdf")


def teardown():
    if os.path.exists(file_path[:-5] + "five"):
        os.remove(file_path[:-5] + "five")
    if os.path.exists(file_path[:-5] + "five.pdf"):
        os.remove(file_path[:-5] + "five.pdf")
