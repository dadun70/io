# Cyclomatic Complexity corresponds to the number of decisions a block of code contains plus 1.
# This number (also called McCabe number) is equal to the number of linearly independent paths through the code.

# Statements have the following effects on Cyclomatic Complexity:

# if 	    +1	An if statement is a single decision.
# elif	    +1	The elif statement adds another decision.
# else	    +0	The else statement does not cause a new decision. The decision is at the if.
# for	    +1	There is a decision at the start of the loop.
# while	    +1	There is a decision at the while statement.
# except 	+1	Each except branch adds a new conditional path of execution.
# finally	+0	The finally block is unconditionally executed.
# with	    +1	The with statement roughly corresponds to a try/except block (see PEP 343 for details).
# assert	    +1	The assert statement internally roughly equals a conditional statement.
# Boolean Operator	+1	Every boolean operator (and, or) adds a decision point.

def compute_cyclomatic_complexity(path):
    counter = 0
    f = open(path, 'r')
    line = f.readline()
    while line:

        if not line.isspace() and not line.startswith('#'):
            words = line.strip().split(' ')
            words = list(filter(None, words))

            if words[0] == 'if' or words[0] == 'elif':
                counter = counter + 1
                for word in words:
                    if word.find('or') != -1 or word.find('and') != -1:
                        counter = counter + 1
            elif words[0] == 'for' or words[0] == 'while' or words[0] == 'except' or words[0] == 'assert':
                counter = counter + 1
        line = f.readline()
    f.close()
    return counter


def rank_fies(filename, number):

    line = ''
    if number <= 5:
       line= filename + ": " +str(number) + "              A (low risk - simple block)\n"
    elif number > 5 and number <= 10:
        line = filename + ": " + str(number) + "           B (low risk - well structured and stable block)\n"
    elif number > 10 and number <= 20:
        line = filename + ": " + str(number) + "           C (moderate risk - slightly complex block)\n"
    elif number > 20 and number <= 30:
        line = filename + ": " + str(number) + "           D (more than moderate risk - more complex block)\n"
    elif number > 30 and number <= 40:
        line = filename + ": " + str(number) + "           E (high risk - complex block, alarming)\n"
    elif number > 40:
        line = filename + ": " + str(number) + "           F (very high risk - error-prone, unstable block)\n"
    else:
        line = filename + ": " + str(number) + "          INVALID DATA OR NO DATA\n"
    return line