from File import File
from Function import Function


# zwraca strukture grafu dla klas w postaci listy z elementami o takiej strukturze:
# [<nazwa pliku 1>, <waga pliku 1>, <nazwa pliku 2>, <waga pliku 2>, <ilosc wystapien takiej relacji>]
# np. ['file1', 20, 'file3', 25, 2]
def build_graph_structure(files_list):
    result_structure = []
    for file in files_list:
        for file_dependency_name in file.dependencies:
            for file_pair in result_structure:
                if find_duplicates(file_pair, file, file_dependency_name):
                    file_pair[4] += 1
                    break
            else:
                size_of_second_file = return_file_size_by_name(files_list, file_dependency_name)
                if size_of_second_file is not None and not file.name.startswith("test"):
                    result_structure.append([file.name, file.size, file_dependency_name,
                                             return_file_size_by_name(files_list, file_dependency_name), 1])
    return result_structure


# zwraca strukture grafu dla funkcji w postaci listy z elementami o takiej strukturze:
# [<nazwa funkcji 1, <ilosc wywolan funkcji 1 w kodzie> <nazwa funkcji 2>, <ilosc wywolan funkcji 2 w kodzie>, <liczba wystapien takiej relacji>]
# np. ['fun1', 1, 'fun3', 2, 2]
def build_functions_graph_structure(files_list):
    all_functions = []
    result_data = []
    for file in files_list:
        if not file.name.startswith("test"):
            all_functions.extend(file.functions)
    for function in all_functions:
        for function_name in function.functions:
            if check_if_element_in_list_by_name(all_functions, function_name):
                for function_pair in result_data:
                    if find_duplicates(function_pair, function, function_name):
                        function_pair[4] += 1
                        break
                else:
                    result_data.append(
                        [function.name, count_function_in_list_by_name(all_functions, function.name), function_name,
                         count_function_in_list_by_name(all_functions, function_name), 1])
    return result_data


def build_functions_files_graph_structure(files_list):
    data = []
    for x in files_list:
        if not x.name.startswith("test"):
            for y in x.functions:
                for z in data:
                    if find_duplicates(z, x, y):
                        z[4] += 1
                        break
                else:
                    if not y.name.startswith("test"):
                        data.append([y.name, "", x.name, "", ""])
    return data


def build_modules_graph_structure(files_list):
    data = []
    for x in files_list:
        for y in x.functions:
            for z in make_list_of_functions(y.functions):
                second_module = find_model_of_function(files_list, z, x.name)
                if second_module == None:
                    continue
                for draw in data:
                    if draw[0] == x.name:
                        if draw[2] == second_module:
                            draw[4] += 1
                            break
                else:
                    if not x.name.startswith("test"):
                        data.append([x.name, "", second_module, "", 1])

    return data


def make_list_of_functions(list):
    data = []
    for x in list:
        duplicate = 0
        for y in data:
            if y == x:
                duplicate = 1
                break
        if duplicate == 0:
            data.append(x)
    return data


def find_model_of_function(files_list, name, name_mod):
    for x in files_list:
        if name_mod == x.name:
            continue
        for y in x.functions:
            if y.name == name:
                return x.name


def find_duplicates(file_pair_data, first_name, second_name):
    return (file_pair_data[0] == first_name.name and file_pair_data[2] == second_name) or (
            file_pair_data[0] == second_name and file_pair_data[2] == first_name.name)


def return_file_size_by_name(files_list, wanted_name):
    for file in files_list:
        if file.name == wanted_name:
            return file.size


def check_if_element_in_list_by_name(list_of_functions, name):
    for function in list_of_functions:
        if function.name == name:
            return True
    else:
        return False


def count_function_in_list_by_name(functions, name):
    counter = 0
    for x in functions:
        if x.name == name:
            counter += 1
    return counter


def gather_three_versions(files_list, data1, data2, data3, story):
    from graphviz import Digraph
    from DrawGraph import view_graph

    g = Digraph('G', filename='five')
    g.attr(compound='true')
    if story.find("2") != -1:
        for x in files_list:
            if not x.name.startswith("test"):
                with g.subgraph(name="cluster_" + x.name) as c:
                    c.attr(label=x.name)
                    c.attr(style='filled', color='lightgrey')
                    c.node_attr.update(style='filled', color='white')
                    for function in x.functions:
                        c.node(function.name)
        for x in data2:
            g.edge(x[0], x[2])

        if story.find("1") != -1:
            for x in data1:
                file1 = find_file_by_name(files_list, x[0])
                file2 = find_file_by_name(files_list, x[2])
                if len(file2.functions):
                    if file1.functions[0].name == "__init__()":
                        node1 = file1.functions[1].name
                    else:
                        node1 = file1.functions[0].name
                    if file2.functions[0].name == "__init__()":
                        node2 = file2.functions[1].name
                    else:
                        node2 = file2.functions[0].name

                    g.edge(node1, node2, str(x[1]), ltail="cluster_" + x[0], lhead="cluster_" + x[2])

        if story.find("3") != -1:
            for x in data3:
                file1 = find_file_by_name(files_list, x[0])
                file2 = find_file_by_name(files_list, x[2])
                if len(file2.functions):
                    if file1.functions[0].name == "__init__()":
                        node1 = file1.functions[1].name
                    else:
                        node1 = file1.functions[0].name
                    if file2.functions[0].name == "__init__()":
                        node2 = file2.functions[1].name
                    else:
                        node2 = file2.functions[0].name

                    g.edge(node1, node2, str(x[4]), ltail="cluster_" + x[0], lhead="cluster_" + x[2])

    else:
        if story.find("1") != -1:
            for x in data1:
                vertex2 = x[0] + "\n" + str(x[1])
                vertex1 = x[2] + "\n" + str(x[3])
                edge = str(x[4])
                if story.find("3") != -1:
                    for module in data3:
                        if module[0] == x[0] and module[2] == x[2]:
                            edge = str(module[4])
                g.edge(vertex2, vertex1, edge)
        else:
            for x in data3:
                vertex2 = x[0] + "\n" + str(x[1])
                vertex1 = x[2] + "\n" + str(x[3])
                edge = str(x[4])
                g.edge(vertex2, vertex1, edge)
    view_graph(g)


def find_file_by_name(files_list, name):
    for x in files_list:
        if x.name == name:
            return x
