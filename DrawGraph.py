#funkcja rysujaca graf, przyjmuje liste z listami
# nalezy zainsatalowac biblioteke Graphviz 2.38 i dodac .../Graphviz 2.38/bin
# do zmiennych srodowiskowych
from graphviz import Digraph
import os

def draw_graph(data, version, name):
	graph = Digraph(comment='Program Dependences Diagram')
	graph.attr('node', shape='box', style='filled', fillcolor='lightblue')

	for x in data:
		vertex1 = x[0] + "\n" + str(x[1])
		vertex2 = x[2] + "\n" + str(x[3])
		edge = str(x[4])
		graph.edge(vertex1, vertex2, edge)

	graph.attr(label="Version: " + str(version))
	graph.attr(fontsize='20')

	#jesli zamiast true damy false to grafy nie beda sie automatycznie wyswietlac
	graph.render(name, view=True, format='svg')


def compare_and_draw(data, currentVersion, oldGraphName):
	graph = Digraph(comment='Program Dependences Diagram')
	graph.attr('node', shape='box', style='filled', fillcolor='lightblue')
	if os.path.exists(oldGraphName + ".svg"):
		svgData = open(oldGraphName + ".svg", 'r').read()
	else:
		return
	v1 = svgData.find("Version:") + 9
	versionLabel = "Comparison of current version(" + currentVersion + ") and " + svgData[v1:v1 + 40]

	vertexList = []
	for x in data:
		vertexList.append(x[0] + "\n" + str(x[1]))
		vertexList.append(x[2] + "\n" + str(x[3]))

	vertexList = list(dict.fromkeys(vertexList))
	for x in vertexList:
		if svgData.find(x) == -1:
			graph.node(x, color="red")
		else:
			graph.node(x, color="black")

	vertexList.clear()
	for x in data:
		vertexList.append(x[0])
		vertexList.append(x[2])

	vertexList = list(dict.fromkeys(vertexList))
	vertexList.sort()
	svgVertexSizeList = []
	for x in vertexList:
		index1 = svgData.find(x) + len(x) + 1
		if index1 != -1:
			index2 = svgData.find(" -->", index1)
			svgVertexSizeList.append(svgData[index1:index2])

	for x in data:
		vertex1 = x[0] + "\n" + str(x[1])
		vertex2 = x[2] + "\n" + str(x[3])
		edge = str(x[4])
		index1 = vertexList.index(x[0])
		svgVertex1 = x[0] + "\n" + svgVertexSizeList[index1]
		index2 = vertexList.index(x[2])
		svgVertex2 = x[2] + "\n" + svgVertexSizeList[index2]

		index1 = svgData.find(svgVertex1 + "&#45;&gt;" + svgVertex2)
		index2 = svgData.find("/text>",index1)
		if index1 == -1:
			graph.edge(vertex2, vertex1, edge, color="red")
		elif svgData.find(">" + edge + "<",index1,index2) == -1:
			graph.edge(vertex2, vertex1, edge, color="red")
		else:
			graph.edge(vertex2, vertex1, edge, color="black")

	graph.attr(label=versionLabel)
	graph.attr(fontsize='10')
	graph.render('Comparison', view=True, format='svg')


def view_graph(graph):
    from graphviz import Digraph
    graph.view()