Żeby zainstalować biblioteki uruchomić:\
`pip install -r requirements.txt`

Aby grafy rysowały się poprawnie należy:
1. Zainstalować do Pythona pakiet graphviz (pip install graphviz).
2. Pobrać i zainstalować Graphviz (https://graphviz.org/download/).
3. Dodać do zmiennych środowiskowych systemu katalog bin programu Graphviz (.../Graphviz2.38/bin).
4. Powinno wszystko działać :D

Nie ma za bardzo funkcji, która rysuje graf, więc skorzystanie z tej biblioteki jest najprostszym sposobem. Jak ktoś nie wierzy, to niech sobie poszuka :P

TESTY
1. Zainstalować biblioteki: pytest i pytest-cov
2. Będąc w folderze projektu wywołac `pytest --cov=.` lub po `--cov=` podać sciężkę do wnętrza folderu projektu