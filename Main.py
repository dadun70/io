import os
import git

from Reader import *
from Graph_service import *
from DrawGraph import *
from Export import exportXML
from File import File
from Complexity import *

def main():
    ROOT_DIR = None
    print("Podaj ścieżkę do katalogu lub enter, jeżeli chcesz wybrać aktualną ścieżkę programu:")
    while (ROOT_DIR == None):
        ROOT_DIR = input()
        if not ROOT_DIR:
            ROOT_DIR = os.path.abspath(os.curdir)
            break
        if not os.path.isdir(ROOT_DIR):
            print("Nieprawidłowa ścieżka, wpisz ponownie:")
            ROOT_DIR = None

    menu = "1 - Polaczenie miedzy plikami\n"
    menu += "2 - Relacje miedzy metodami\n"
    menu += "3 - Relacje miedzy modulami logicznymi\n"
    menu += "4 - Export do Visual Paradigm\n"
    menu += "5 - Warstwowa wizualizacja grafow\n"
    menu += "6 - Relacje miedzy plikami a metodami\n"
    menu += "7 - Zlozonosc cyklomatyczna\n"
    menu += "8 - Porownanie wersji grafu\n"
    menu += "9 - Zamknij program\n"
    dirs = find_file(ROOT_DIR)
    file_table = []

    for file in dirs:
        x = parse_file(file)
        file_table.append(File(x.name, x.size, x.dependencies, x.functions))

    if len(file_table) == 0:
        input("Nie znaleziono plików z kodem napisanym w pythonie \nWciśnij przycisk aby zakończyć...")
        exit()

    repo = git.Repo(search_parent_directories=True, path=ROOT_DIR)

    while (True):
        os.system('cls' if os.name == 'nt' else 'clear')
        story_number = input(menu)
        if story_number == "1":
            data = build_graph_structure(file_table)
            draw_graph(data, repo.head.object.hexsha, "story1")
        elif story_number == "2":
            data = build_functions_graph_structure(file_table)
            draw_graph(data, repo.head.object.hexsha, "story2")
        elif story_number == "3":
            data = build_modules_graph_structure(file_table)
            draw_graph(data, repo.head.object.hexsha, "story3")
        elif story_number == "4":
            data = build_graph_structure(file_table)
            exportXML("story1", data)
            data = build_functions_graph_structure(file_table)
            exportXML("story2", data)
            data = build_modules_graph_structure(file_table)
            exportXML("story3", data)
        elif story_number == "5":
            data1 = build_graph_structure(file_table)
            data2 = build_functions_graph_structure(file_table)
            data3 = build_modules_graph_structure(file_table)
            while 1 == 1:
                story = input("Choose a story(example: \"12\" or \"23\"), if u want back to main type \"exit\":")
                if story == "exit":
                    break
                gather_three_versions(file_table, data1, data2, data3, story)
        elif story_number == "6":
            data = build_functions_files_graph_structure(file_table)
            draw_graph(data, repo.head.object.hexsha, "story6")
        elif story_number == "7":
            lines_table = []

            for path in dirs:
                counter = compute_cyclomatic_complexity(path)
                lines_table.append(rank_fies(path, counter))

            given_path = input('Give path to the directory where result should be saved:\n')
            if not  given_path:
                given_path = "C:\\Temp"

            if not os.path.exists(given_path):
                os.makedirs(given_path)

            plik = open(given_path + '/' + 'CyclomaticComplexity.txt', 'w')
            for line in lines_table:
                plik.writelines(line)

            plik.close()

        elif story_number == "8":
            oldGraphName = input("Podaj nazwe starego grafu:\n")
            story = input("Podaj historyjke (1-3):\n")
            if story == "1":
                data = build_graph_structure(file_table)
            elif story == "2":
                data = build_functions_graph_structure(file_table)
            elif story == "3":
                data = build_modules_graph_structure(file_table)
            else:
                continue
            compare_and_draw(data, repo.head.object.hexsha, oldGraphName)

        elif story_number == "9":
            break
if __name__ == '__main__':
   main()