def exportXML(file_name, data):

	# przygotowanie danych
	vertexList = []
	edgesFromList = []
	edgesToList = []
	for item in data:
		vertexList.append(item[0])
		vertexList.append(item[2])
		edgesFromList.append(item[0])
		edgesToList.append(item[2])

	vertexList=list(dict.fromkeys(vertexList))

	xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Project Xml_structure=\"simple\">\n"
	model = "<Models>\n"
	diagram = "<Diagrams>\n<ClassDiagram Name=\"" + file_name + "\">\n<Shapes>"

	# model klas
	for vertex in vertexList:
		model+="<Class Id=\"ID" + vertex + "\" Name=\"" + vertex + "\">\n<MasterView>\n"
		model+="<Class Idref=\"IDMaster" + vertex + "\" Name=\"" + vertex + "\"/>\n</MasterView>\n</Class>\n"

	# model relacji
	model+="<ModelRelationshipContainer>\n<ModelChildren>\n"
	i = 0
	while i < len(edgesFromList):
		model+="<Usage From=\"ID" + edgesFromList[i] + "\" Id=\"ID" + edgesFromList[i] + "To" + edgesToList[i] + "\" To=\"ID" + edgesToList[i] + "\">\n</Usage>\n"
		i+=1

	model+="</ModelChildren>\n</ModelRelationshipContainer>\n</Models>\n"

	# diagram klas
	for vertex in vertexList:
		diagram+="<Class Id=\"IDMaster" + vertex + "\" Model=\"ID" + vertex + "\" Name=\"" + vertex + "\">\n</Class>\n"

	diagram+="</Shapes>\n<Connectors>\n"

	# diagram relacji
	i = 0
	while i < len(edgesFromList):
		diagram+="<Usage From=\"IDMaster" + edgesFromList[i] + "\" Model=\"ID" + edgesFromList[i] + "To" + edgesToList[i] + "\" To=\"IDMaster" + edgesToList[i] + "\">\n</Usage>\n"
		i+=1

	diagram+="</Connectors>\n</ClassDiagram>\n</Diagrams>\n"

	xml+=model + diagram + "</Project>"
	file = open(file_name + ".xml", "w")
	file.write(xml)
	file.close()